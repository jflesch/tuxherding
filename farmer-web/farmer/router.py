import ldapdb.router


class LdapRouter(ldapdb.router.Router):
    """ Workaround """
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        # TODO(Jflesch): UGLY
        if model_name is not None and "ldap" in model_name.lower():
            return False
        return None
