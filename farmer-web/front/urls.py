from django.urls import path

from .views import (
    home,
    install
)

app_name = 'front'

urlpatterns = [
    path('', home, name="home"),
    path('install', install, name="install"),
]
