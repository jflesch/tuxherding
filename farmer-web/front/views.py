from django.shortcuts import render


def home(request):
    return render(request, 'home.html', {})


def install(request):
    return render(request, 'install.html', {})
