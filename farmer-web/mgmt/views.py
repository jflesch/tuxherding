import datetime
import json
import os
import random
import shutil
import string
import time

import dulwich.porcelain
import petname

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import (
    HttpResponse,
    HttpResponseRedirect,
    JsonResponse
)
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from . import forms
from . import models
from . import ppgen


def generate_passwords(length=16, nb_passwords=6):
    for _ in range(0, nb_passwords):
        characters = string.ascii_letters + string.digits
        yield "".join(random.choice(characters) for _ in range(length))


def get_next_ldap_uid_number(cls=models.LdapUser, default=10000):
    try:
        user = cls.objects.order_by("-uid")[0]
        return user.uid + 1
    except IndexError:
        return default


def get_storage_server_assignment(user):
    try:
        return models.StorageServerAssignment.objects.get(user_cn=user.cn)
    except models.StorageServerAssignment.DoesNotExist:
        return models.StorageServerAssignment(
            user_cn=user.cn,
            server=models.StorageServer.objects.order_by('nice_name')[0]
        )


@staff_member_required
def home(request):
    unread_errors = models.ErrorLog.objects.defer('content').filter(
        read=False
    ).count()
    return render(request, 'mgmt_home.html', {
        'unread_errors': unread_errors,
    })


@staff_member_required
def create_user(request):
    servers = models.StorageServer.objects.order_by('nice_name')
    if request.method == "POST":
        form = forms.LdapUserForm(servers, None, request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.cn = user.user_name
            user.home_directory = models.HOME_PATHS.format(user.user_name)
            user.uid = get_next_ldap_uid_number()
            user.group = 10000
            user.save()

            server_id = form.cleaned_data['server']
            server = models.StorageServer.objects.get(id=server_id)

            assign = get_storage_server_assignment(user)
            assign.server = server
            assign.save()

            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_users")
            )
    else:
        form = forms.LdapUserForm(servers, None)
    return render(request, 'mgmt_user.html', {
        "name": "",
        "user": form,
        "target_url": reverse("mgmt:create_user"),
        "action": "Ajouter",
        "suggested_passwords":
            list(generate_passwords(12)) + list(ppgen.generate_passwords()),
    })


@staff_member_required
def edit_user(request, user_cn):
    servers = models.StorageServer.objects.order_by('nice_name')
    user = models.LdapUser.objects.get(cn=user_cn)
    assign = get_storage_server_assignment(user)
    computers = get_user_computers(user_cn)

    if request.method == "POST":
        passwd = user.password
        form = forms.LdapUserForm(
            servers, None, request.POST, instance=user
        )
        if form.is_valid():
            user = form.save(commit=False)
            user.cn = user.user_name
            user.home_directory = models.HOME_PATHS.format(user.user_name)
            if len(user.password) <= 0:
                user.password = passwd
            user.save()

            server_id = form.cleaned_data['server']
            assign.server = models.StorageServer.objects.get(id=server_id)
            assign.save()

            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_users")
            )
    else:
        form = forms.LdapUserForm(
            servers, assign.server, instance=user
        )
    return render(request, 'mgmt_user.html', {
        "computers": computers,
        "name": user.user_name,
        "user": form,
        "target_url": reverse("mgmt:edit_user", args=[user.cn]),
        "action": "Modifier",
        "suggested_passwords":
            list(generate_passwords(12)) + list(ppgen.generate_passwords()),
    })


@staff_member_required
def list_users(request):
    users = models.LdapUser.objects.all()

    # we cannot sort LDAP objects in a case-insensitive manner using .extra()
    # so we do it manually
    users = [(u.full_name.lower(), idx, u) for (idx, u) in enumerate(users)]
    users.sort()
    users = [u[2] for u in users]

    return render(request, 'mgmt_users.html', {
        "users": users
    })


@staff_member_required
def list_errors(request):
    errors = models.ErrorLog.objects.all().defer('content').order_by(
        "-date"
    )[:50]
    return render(request, 'mgmt_errors.html', {
        "errors": errors
    })


@login_required
def show_error(request, error_id):
    error = models.ErrorLog.objects.get(id=error_id)
    if not request.user.is_staff:
        check_user_access(request, error.computer_cn)
    try:
        content = json.loads(error.content)
    except json.JSONDecodeError:
        content = error.content
    if request.user.is_staff:
        error.read = True
        error.save()
    return render(request, 'mgmt_error.html', {
        "name": error.computer_cn,
        "error": error,
        "content": content,
    })


@login_required
def show_dmesg(request, dmesg_id):
    dmesg = models.Dmesg.objects.get(id=dmesg_id)
    if not request.user.is_staff:
        check_user_access(request, dmesg.computer_cn)
    try:
        content = json.loads(dmesg.content)
    except json.JSONDecodeError:
        content = dmesg.content
    return render(request, 'mgmt_dmesg.html', {
        "name": dmesg.computer_cn,
        "dmesg": dmesg,
        "content": content,
    })


@staff_member_required
def clear_errors(request):
    models.ErrorLog.objects.all().update(read=True)
    return HttpResponseRedirect(
        redirect_to=reverse("mgmt:list_errors")
    )


@staff_member_required
def create_variable(request, computer_cn=""):
    if request.method == "POST":
        form = forms.VariableForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:home")
            )
    else:
        form = forms.VariableForm(initial={
            'computer_cn': computer_cn,
        })

    return render(request, 'mgmt_variable.html', {
        "name": "",
        "variable": form,
        "target_url": reverse("mgmt:create_variable"),
        "action": "Ajouter",
    })


@staff_member_required
def edit_variable(request, variable_id):
    variable = models.Variable.objects.get(id=int(variable_id))
    if request.method == "POST":
        form = forms.VariableForm(request.POST, instance=variable)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:home")
            )
    else:
        form = forms.VariableForm(instance=variable)
    return render(request, 'mgmt_variable.html', {
        "variable_id": str(variable.id),
        "name": "{} {}".format(variable.computer_cn, variable.name),
        "variable": form,
        "target_url": reverse("mgmt:edit_variable", args=[variable.id]),
        "action": "Modifier",
    })


@staff_member_required
def delete_variable(request, variable_id):
    models.Variable.objects.get(id=int(variable_id)).delete()
    return HttpResponseRedirect(redirect_to=reverse("mgmt:home"))


@staff_member_required
def list_variables(request):
    variables = models.Variable.objects.filter(
        computer_cn=""
    ).order_by("name")
    return render(request, 'mgmt_variables.html', {
        "variables": variables
    })


def get_computer_users(computer):
    assignments = models.ComputerAssignment.objects.filter(
        computer_cn=computer.cn
    )
    users = []
    for assignment in assignments:
        try:
            user = models.LdapUser.objects.get(cn=assignment.user_cn)
            users.append(user)
        except models.LdapUser.DoesNotExist:
            pass
    return users


def get_user_computers(username):
    assignments = models.ComputerAssignment.objects.filter(
        user_cn=username
    ).order_by("computer_cn")
    computers = []
    for assignment in assignments:
        try:
            computer = models.LdapComputer.objects.get(
                cn=assignment.computer_cn
            )
            computers.append(computer)
        except models.LdapComputer.DoesNotExist:
            pass
    return computers


def update_computer_users(computer, users):
    models.ComputerAssignment.objects.filter(computer_cn=computer.cn).delete()
    for user in users:
        a = models.ComputerAssignment(computer_cn=computer.cn, user_cn=user.cn)
        a.save()


def get_computer_roles(computer):
    assignments = models.RoleAssignment.objects.filter(
        computer_cn=computer.cn
    ).order_by("role__name")
    return [a.role for a in assignments]


def update_computer_roles(computer, roles):
    models.RoleAssignment.objects.filter(computer_cn=computer.cn).delete()
    for role in roles:
        models.RoleAssignment(computer_cn=computer.cn, role=role).save()


@staff_member_required
def create_computer(request):
    name = ""
    if request.method == "POST":
        form = forms.LdapComputerForm(request.POST)
        if form.is_valid():
            computer = form.save(commit=False)
            computer.cn = computer.user_name
            computer.home_directory = "/dev/null"
            computer.uid = get_next_ldap_uid_number(
                cls=models.LdapComputer, default=30000
            )
            computer.group = 30000
            computer.save()
            update_computer_users(computer, form.cleaned_data['users'])
            update_computer_roles(computer, form.cleaned_data['roles'])
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_computers")
            )
    else:
        name = petname.generate()
        name = name.split("-")
        name = [f.title() for f in name]
        name = "".join(name)

        form = forms.LdapComputerForm(
            users=[], initial={'user_name': name}
        )

    return render(request, 'mgmt_computer.html', {
        "name": name,
        "computer": form,
        "target_url": reverse("mgmt:create_computer"),
        "action": "Ajouter",
        "address": "",
        "suggested_passwords": generate_passwords(24),
    })


def get_computer_stats(computer):
    stats = models.Statistic.objects.filter(computer_cn=computer.cn).order_by(
        "-date", "name"
    )
    out = []
    max_dates = 4
    for stat in stats:
        if len(out) <= 0 or out[-1]['date'] != stat.date:
            max_dates -= 1
            if max_dates <= 0:
                break
            out.append({"date": stat.date, "stats": []})
        out[-1]['stats'].append(stat)
    return out


@login_required
def edit_computer(request, computer_cn):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)

    computer = models.LdapComputer.objects.get(cn=computer_cn)
    if request.method == "POST":
        if not request.user.is_staff:
            raise PermissionDenied("Forbidden")
        passwd = computer.password
        form = forms.LdapComputerForm(request.POST, instance=computer)
        if form.is_valid():
            computer = form.save(commit=False)
            computer.cn = computer.user_name
            if len(computer.password) <= 0:
                computer.password = passwd
            computer.save()
            update_computer_users(computer, form.cleaned_data['users'])
            update_computer_roles(computer, form.cleaned_data['roles'])
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_computers")
            )
    elif request.user.is_staff:
        form = forms.LdapComputerForm(
            users=get_computer_users(computer),
            roles=get_computer_roles(computer),
            instance=computer
        )
    else:
        form = computer
    variables = models.Variable.objects.filter(
        computer_cn=computer.cn
    ).order_by("name")
    unread_errors = models.ErrorLog.objects.defer('content').filter(
        computer_cn=computer_cn
    ).filter(read=False).count()
    try:
        address = models.ComputerAddress.objects.get(
            computer_cn=computer.cn
        ).address
    except models.ComputerAddress.DoesNotExist:
        address = ""
    return render(request, 'mgmt_computer.html', {
        "action": "Modifier",
        "address": address,
        "computer": form,
        "name": computer.cn,
        "suggested_passwords": generate_passwords(24),
        "target_url": reverse("mgmt:edit_computer", args=[computer.cn]),
        "unread_errors": unread_errors,
        "variables": variables,
    })


@login_required
def list_computers(request):
    if request.user.is_staff:
        computers = models.LdapComputer.objects.all()
    else:
        computers = get_user_computers(request.user.username)

    # we cannot sort LDAP objects in a case-insensitive manner using .extra()
    # so we do it manually
    computers = [
        (c.user_name.lower(), idx, c)
        for (idx, c) in enumerate(computers)
    ]
    computers.sort()
    computers = [c[2] for c in computers]

    for computer in computers:
        computer.users = get_computer_users(computer)
    return render(request, 'mgmt_computers.html', {
        "computers": computers,
    })


@staff_member_required
def create_server(request):
    if request.method == "POST":
        form = forms.StorageServerForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_servers")
            )
    else:
        form = forms.StorageServerForm()
    return render(request, 'mgmt_server.html', {
        "server": form,
        "target_url": reverse("mgmt:create_server"),
        "action": "Ajouter",
    })


@staff_member_required
def edit_server(request, server_id):
    server = models.StorageServer.objects.get(id=server_id)
    if request.method == "POST":
        form = forms.StorageServerForm(request.POST, instance=server)
        if form.is_valid():
            server = form.save()
            return HttpResponseRedirect(
                redirect_to=reverse("mgmt:list_servers")
            )
    else:
        form = forms.StorageServerForm(instance=server)
    return render(request, 'mgmt_server.html', {
        "name": server.nice_name,
        "server": form,
        "target_url": reverse("mgmt:edit_server", args=[server.id]),
        "action": "Modifier",
    })


@staff_member_required
def list_servers(request):
    servers = models.StorageServer.objects.order_by('nice_name')
    return render(request, 'mgmt_servers.html', {
        "servers": servers
    })


def check_computer_login(request):
    login = request.POST.get('computer_login', '')
    passwd = request.POST.get('computer_passwd', '')
    try:
        computer = models.LdapComputer.objects.get(user_name=login)
    except models.LdapUser.DoesNotExist:
        raise PermissionDenied("Forbidden")
    if computer.password != models.compute_ldap_password_hash(
                passwd,
                refpasswd=computer.password
            ):
        raise PermissionDenied("Forbidden")
    return computer


def check_user_access(request, computer_cn):
    try:
        models.ComputerAssignment.objects.get(
            user_cn=request.user.username,
            computer_cn=computer_cn
        )
    except models.ComputerAssignment.DoesNotExist:
        raise PermissionDenied("Forbidden")


@csrf_exempt
def get_computer_vars(request, computer_cn):
    computer = check_computer_login(request)
    users = get_computer_users(computer)
    cvars = {
        "computer_name": computer.cn,
        "users": [user.user_name for user in users],
    }
    return JsonResponse(cvars)


@staff_member_required
def list_roles(request):
    roles = models.Role.objects.order_by('name')
    return render(request, 'mgmt_roles.html', {
        "repository": settings.GIT_CLONE_REPOSITORY,
        "roles": roles,
    })


@login_required
def show_stat(request, computer_cn, stat_id):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)
    stat = models.Statistic.objects.get(id=stat_id)
    if stat.computer_cn != computer_cn:
        raise PermissionDenied("Forbidden")

    stats = models.Statistic.objects.filter(
        computer_cn=computer_cn
        ).filter(name=stat.name).order_by("date")[:30]

    xdata = [
        (time.mktime(s.date.timetuple()) * 1000)
        for s in stats
    ]

    factor = 1000
    for s in stats:
        if s.value < 1000:
            factor = 1
            break

    ydata = [s.value / factor for s in stats]

    tooltip_date = "%Y %m %d %H:%M:%S %p"
    extra_serie = {
        "tooltip": {
            "y_start": "",
            "y_end": ""
        },
        "date_format": tooltip_date
    }

    chartdata = {
        'x': xdata,
        'name1': 'value',
        'y1': ydata,
        'extra1': extra_serie,
    }

    data = {
        'computer_cn': computer_cn,
        'name': stat.name,
        'charttype': 'lineChart',
        'chartdata': chartdata,
        'chartcontainer': 'linewithfocuschart_container',
        'extra': {
            'x_is_date': True,
            'x_axis_format': '%Y %m %d %H',
            'tag_script_js': True,
            'jquery_on_ready': False,
        }
    }

    return render(request, 'mgmt_stat.html', data)


@staff_member_required
def reload_roles(requiest):
    old_roles = {r.name for r in models.Role.objects.all()}
    new_roles = set()

    if os.path.exists(os.path.join(settings.GIT_CLONE_BASEPATH, ".git")):
        try:
            repo = dulwich.porcelain.Repo(settings.GIT_CLONE_BASEPATH)
            dulwich.porcelain.pull(repo, settings.GIT_CLONE_REPOSITORY)
        except Exception as exc:
            shutil.rmtree(settings.GIT_CLONE_BASEPATH)
            dulwich.porcelain.clone(
                settings.GIT_CLONE_REPOSITORY,
                settings.GIT_CLONE_BASEPATH
            )
    else:
        dulwich.porcelain.clone(
            settings.GIT_CLONE_REPOSITORY,
            settings.GIT_CLONE_BASEPATH
        )

    git_roles = os.listdir(os.path.join(settings.GIT_CLONE_BASEPATH, "roles"))

    nb_roles = 0
    for role in git_roles:
        if role.startswith("."):
            continue
        nb_roles += 1
        if role in old_roles:
            old_roles.remove(role)
        else:
            new_roles.add(role)

    print("[git] {} roles found in Git repository".format(nb_roles))

    for role in old_roles:
        # drop removed roles
        print("[git] dropping role {}".format(role))
        models.Role.objects.get(name=role).delete()

    for role in new_roles:
        print("[git] adding role {}".format(role))
        models.Role(name=role).save()

    return HttpResponseRedirect(
        redirect_to=reverse("mgmt:list_roles")
    )


@csrf_exempt
def get_playbook_inventory(request, computer_cn):
    computer = check_computer_login(request)

    roles = get_computer_roles(computer)
    roles = [str(p.name) for p in roles]

    users = get_computer_users(computer)
    users = [str(user.user_name) for user in users]

    variables = models.Variable.objects.filter(computer_cn="")
    v = {var.name: var.value for var in variables}
    variables = models.Variable.objects.filter(computer_cn=computer.cn)
    v.update({var.name: var.value for var in variables})

    v.update({
        "repository": settings.GIT_CLONE_REPOSITORY,
        "roles": roles,
        "users": users,
    })

    return JsonResponse(v)


@csrf_exempt
def post_stats(request, computer_cn):
    computer = check_computer_login(request)
    now = datetime.datetime.now()

    stats = json.loads(request.POST['stats'])
    for stat in stats:
        try:
            s = models.Statistic(
                computer_cn=computer.cn,
                date=now,
                name=stat['name'],
                value=int(stat['value'])
            )
            s.save()
        except Exception as exc:
            print("Failed to add stat [{}] from {}: {}".format(
                str(stat), computer_cn, str(exc)
            ))
    return HttpResponse("OK")


@csrf_exempt
def post_address(request, computer_cn):
    computer = check_computer_login(request)
    models.ComputerAddress.objects.filter(computer_cn=computer.cn).delete()
    models.ComputerAddress(
        computer_cn=computer.cn,
        address=request.POST['address']
    ).save()
    return HttpResponse("OK")


@csrf_exempt
def post_error(request, computer_cn):
    computer = check_computer_login(request)
    models.ErrorLog(
        computer_cn=computer.cn,
        content=request.POST['log']
    ).save()
    return HttpResponse("OK")


@csrf_exempt
def post_dmesg(request, computer_cn):
    computer = check_computer_login(request)
    models.Dmesg(
        computer_cn=computer.cn,
        content=request.POST['log']
    ).save()
    return HttpResponse("OK")


@csrf_exempt
def post_upgrade(request, computer_cn):
    ISO_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

    date = datetime.datetime.strptime(request.POST['date'], ISO_FORMAT)
    computer = check_computer_login(request)
    pkgs = json.loads(request.POST['pkgs'])
    for (pkg, version) in pkgs:
        models.Upgrade(
            computer_cn=computer.cn,
            date=date,
            pkg=pkg[:127],
            version=version[:127],
            command=request.POST['command'][:127]
        ).save()
    return HttpResponse("OK")


@login_required
def show_stats(request, computer_cn):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)
    computer = models.LdapComputer.objects.get(cn=computer_cn)
    stats = get_computer_stats(computer)
    return render(request, 'mgmt_stats.html', {
        "name": computer.cn,
        "stats": stats,
    })


@login_required
def show_computer_errors(request, computer_cn):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)

    computer = models.LdapComputer.objects.get(cn=computer_cn)
    errors = models.ErrorLog.objects.defer('content').filter(
        computer_cn=computer.cn
    ).order_by("-date")[:50]
    return render(request, 'mgmt_computer_errors.html', {
        "action": "Modifier",
        "errors": errors,
        "name": computer.cn,
    })


@login_required
def show_computer_dmesgs(request, computer_cn):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)

    computer = models.LdapComputer.objects.get(cn=computer_cn)
    dmesgs = models.Dmesg.objects.defer('content').filter(
        computer_cn=computer.cn
    ).order_by("-date")
    return render(request, 'mgmt_computer_dmesgs.html', {
        "dmesgs": dmesgs,
        "name": computer.cn,
    })


@login_required
def show_computer_upgrades(request, computer_cn):
    if not request.user.is_staff:
        check_user_access(request, computer_cn)

    computer = models.LdapComputer.objects.get(cn=computer_cn)
    upgrades = models.Upgrade.objects.filter(computer_cn=computer.cn).order_by(
        "-date"
    )
    return render(request, 'mgmt_computer_upgrades.html', {
        "upgrades": upgrades,
        "name": computer.cn,
    })


@login_required
def show_user(request):
    user = models.LdapUser.objects.get(cn=request.user.username)
    computers = get_user_computers(request.user.username)
    storage = get_storage_server_assignment(user)
    return render(request, 'mgmt_show_user.html', {
        'computers': computers,
        'ldap': user,
        'storage': storage,
    })
