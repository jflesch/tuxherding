# Generated by Django 2.1.8 on 2019-05-26 13:04

from django.db import migrations
import ldapdb.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mgmt', '0004_auto_20190526_1303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ldapuser',
            name='postalCode',
            field=ldapdb.models.fields.IntegerField(blank=True, db_column='postalCode', verbose_name='Code postal'),
        ),
    ]
