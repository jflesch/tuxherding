# Generated by Django 2.1.8 on 2019-06-02 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mgmt', '0012_auto_20190602_1241'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ComputerMail',
        ),
        migrations.AlterField(
            model_name='errorlog',
            name='content',
            field=models.TextField(),
        ),
    ]
