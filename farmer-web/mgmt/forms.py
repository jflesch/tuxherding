from django import forms

from . import models


class LdapUserForm(forms.ModelForm):
    class Meta:
        model = models.LdapUser
        fields = [
            'user_name',
            'full_name',
            'email',
            'phone',
            'postalAddress',
            'postalCode',
            'city',
            'password',
        ]
        widgets = {
            'email': forms.EmailInput(),
            'postalAddress': forms.Textarea(attrs={"cols": 40, "rows": 3}),
            'password': forms.PasswordInput(),
        }

    def __init__(self, servers=None, server=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        servers = (
            (server.id, server.nice_name)
            for server in servers
        )
        server = server.id if server is not None else 0
        self.fields['server'] = forms.ChoiceField(
            label="Serveur", widget=forms.RadioSelect,
            choices=servers, required=True, initial=server
        )

    password2 = forms.CharField(
        label="Mot de passe (confirmation)", widget=forms.PasswordInput,
        required=False
    )

    def clean(self):
        cleaned_data = super().clean()

        passwd = cleaned_data['password']
        passwd2 = cleaned_data['password2']

        if passwd != passwd2:
            raise forms.ValidationError(
                "Les mots de passe ne correspondent pas"
            )

        if not passwd.startswith(models.LDAP_HASH_PREFIX):
            passwd = models.compute_ldap_password_hash(passwd)
            cleaned_data['password'] = passwd
            cleaned_data['password2'] = passwd

        return cleaned_data


class LdapComputerForm(forms.ModelForm):
    class Meta:
        model = models.LdapComputer
        fields = [
            'user_name',
            'full_name',
            'long_description',
            'password',
        ]
        widgets = {
            'password': forms.PasswordInput(),
            'long_description': forms.Textarea(attrs={"cols": 60, "rows": 10}),
        }

    password2 = forms.CharField(
        label="Mot de passe (confirmation)", widget=forms.PasswordInput,
        required=False
    )
    users = forms.ModelMultipleChoiceField(
        queryset=models.LdapUser.objects.all()
    )
    roles = forms.ModelMultipleChoiceField(
        queryset=models.Role.objects.all()
    )

    def __init__(self, *args, users=[], roles=[], **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['users'] = forms.ModelMultipleChoiceField(
            queryset=models.LdapUser.objects.all(),
            widget=forms.SelectMultiple(attrs={
                'size': 15,
                'style': "width: 200px;"
            }),
            required=False,
            initial=users
        )
        self.fields['roles'] = forms.ModelMultipleChoiceField(
            queryset=models.Role.objects.all(),
            widget=forms.CheckboxSelectMultiple,
            required=False,
            initial=roles
        )

    def clean(self):
        cleaned_data = super().clean()

        passwd = cleaned_data['password']
        passwd2 = cleaned_data['password2']

        if passwd != passwd2:
            raise forms.ValidationError(
                "Les mots de passe ne correspondent pas"
            )

        if not passwd.startswith(models.LDAP_HASH_PREFIX):
            passwd = models.compute_ldap_password_hash(passwd)
            cleaned_data['password'] = passwd
            cleaned_data['password2'] = passwd

        return cleaned_data


class StorageServerForm(forms.ModelForm):
    class Meta:
        model = models.StorageServer
        fields = [
            'address',
            'nice_name',
        ]


class VariableForm(forms.ModelForm):
    class Meta:
        model = models.Variable
        fields = [
            'computer_cn',
            'name',
            'value',
        ]
