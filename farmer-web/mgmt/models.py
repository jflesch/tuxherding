from django.conf import settings
from django.db import models

import base64
import hashlib
import os

import ldapdb.models
import ldapdb.models.fields


HOME_PATHS = "/home/ldap/{}"
LDAP_HASH_PREFIX = "{SSHA}"


def compute_ldap_password_hash(passwd_txt, refpasswd=None):
    if len(passwd_txt) <= 0:
        return passwd_txt
    if refpasswd is None:
        salt = os.urandom(4)
    else:
        h = base64.b64decode(refpasswd)
        salt = h[-4:]
    h = hashlib.sha1(str(passwd_txt).encode("UTF-8"))
    h.update(salt)
    h = h.digest()
    h = base64.b64encode(h + salt).strip().decode("UTF-8")
    return "{SSHA}" + h


class LdapUser(ldapdb.models.Model):
    """
    Class for representing a LDAP user entry.
    """
    # LDAP meta-data
    base_dn = "ou=users," + settings.LDAP_ROOT_DN
    object_classes = ['posixAccount', 'shadowAccount', 'inetOrgPerson']

    last_modified = ldapdb.models.fields.DateTimeField(
        db_column='modifyTimestamp', auto_now=True
    )

    # inetOrgPerson
    full_name = ldapdb.models.fields.CharField("Nom complet", db_column='sn')
    cn = ldapdb.models.fields.CharField(db_column='cn')
    email = ldapdb.models.fields.CharField(db_column='mail', blank=True)
    phone = ldapdb.models.fields.CharField(
        "Téléphone", db_column='telephoneNumber', blank=True
    )
    mobile_phone = ldapdb.models.fields.CharField(
        db_column='mobile', blank=True
    )
    photo = ldapdb.models.fields.ImageField(db_column='jpegPhoto')
    postalAddress = ldapdb.models.fields.CharField(
        "Numéro et rue", db_column='homePostalAddress', blank=True
    )
    postalCode = ldapdb.models.fields.IntegerField(
        "Code postal", db_column='postalCode', blank=True
    )
    city = ldapdb.models.fields.CharField(
        "Ville", db_column='l', blank=True
    )

    # posixAccount
    uid = ldapdb.models.fields.IntegerField(db_column='uidNumber', unique=True)
    group = ldapdb.models.fields.IntegerField(db_column='gidNumber')
    gecos = ldapdb.models.fields.CharField(db_column='gecos')
    home_directory = ldapdb.models.fields.CharField(db_column='homeDirectory')
    login_shell = ldapdb.models.fields.CharField(
        db_column='loginShell', default='/usr/bin/rssh'
    )
    user_name = ldapdb.models.fields.CharField(
        "Identifiant", db_column='uid', primary_key=True
    )
    password = ldapdb.models.fields.CharField(
        "Mot de passe", db_column='userPassword', blank=True
    )

    # shadowAccount
    last_password_change = ldapdb.models.fields.TimestampField(
        db_column='shadowLastChange', auto_now_add=True
    )

    def __str__(self):
        return self.user_name

    def __unicode__(self):
        return self.full_name


class LdapComputer(ldapdb.models.Model):
    """
    Class for representing a LDAP computer entry.
    """
    # LDAP meta-data
    base_dn = "ou=computers," + settings.LDAP_ROOT_DN
    object_classes = ['posixAccount', 'shadowAccount', 'inetOrgPerson']

    last_modified = ldapdb.models.fields.DateTimeField(
        db_column='modifyTimestamp', auto_now=True
    )

    # inetOrgPerson
    full_name = ldapdb.models.fields.CharField(
        "Description courte", db_column='sn'
    )
    cn = ldapdb.models.fields.CharField(db_column='cn')
    long_description = ldapdb.models.fields.CharField(
        "Description longue", db_column='homePostalAddress', blank=True
    )

    # posixAccount
    uid = ldapdb.models.fields.IntegerField(db_column='uidNumber', unique=True)
    group = ldapdb.models.fields.IntegerField(db_column='gidNumber')
    home_directory = ldapdb.models.fields.CharField(db_column='homeDirectory')
    user_name = ldapdb.models.fields.CharField(
        "Identifiant", db_column='uid', primary_key=True
    )
    password = ldapdb.models.fields.CharField(
        "Mot de passe", db_column='userPassword', blank=True
    )

    # shadowAccount
    last_password_change = ldapdb.models.fields.TimestampField(
        db_column='shadowLastChange', auto_now_add=True
    )

    def __str__(self):
        return self.user_name

    def __unicode__(self):
        return self.full_name


class StorageServer(models.Model):
    address = models.fields.CharField("Adresse", max_length=128, unique=True)
    nice_name = models.fields.CharField("Nom", max_length=128, unique=True)

    def __str__(self):
        return self.nice_name


class StorageServerAssignment(models.Model):
    """
    I'm too lazy to make a LDAP class, so I use this table instead
    """
    user_cn = models.CharField(max_length=128, unique=True)
    server = models.ForeignKey(StorageServer, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {}".format(self.user_cn, self.server)


class ComputerAssignment(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True)
    user_cn = models.CharField(max_length=128, db_index=True)


class Role(models.Model):
    """
    Ansible roles
    """
    name = models.CharField(max_length=128, db_index=True)

    def __str__(self):
        return str(self.name)


class RoleAssignment(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    computer_cn = models.CharField(max_length=128, db_index=True)


class Variable(models.Model):
    # computer_cn = "" : all computers
    computer_cn = models.CharField(max_length=128, blank=True, null=False)
    name = models.CharField(max_length=128, blank=False, null=False)
    value = models.CharField(max_length=128, blank=False, null=False)

    def __str__(self):
        computer = "all"
        if self.computer_cn is not None and self.computer_cn != "":
            computer = self.computer_cn
        return "[{}] {}={}".format(computer, self.name, self.value)


class Statistic(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True)
    name = models.CharField(max_length=128, db_index=True)
    date = models.DateTimeField(db_index=True)
    value = models.IntegerField()

    def __str__(self):
        return "{} - {} - {}".format(self.computer_cn, self.name, self.date)


class ComputerAddress(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True, unique=True)
    address = models.CharField(max_length=128)

    def __str__(self):
        return "Address of {}".format(self.computer_cn)


class ErrorLog(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True)
    date = models.DateTimeField(db_index=True, auto_now_add=True)
    content = models.TextField(null=False, blank=False, editable=True)
    read = models.BooleanField(null=False, default=False)

    def __str__(self):
        return "ErrorLog {} {}".format(self.date, self.computer_cn)


class Dmesg(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True)
    date = models.DateTimeField(db_index=True, auto_now_add=True)
    content = models.TextField(null=False, blank=False, editable=True)

    def __str__(self):
        return "Dmesg {} {}".format(self.date, self.computer_cn)


class Upgrade(models.Model):
    computer_cn = models.CharField(max_length=128, db_index=True)
    date = models.DateTimeField(db_index=True)
    pkg = models.CharField(
        max_length=128, db_index=True, null=False, blank=False
    )
    version = models.CharField(
        max_length=128, db_index=True, null=False, blank=False
    )
    command = models.CharField(
        max_length=128, null=False, blank=False
    )

    def __str__(self):
        return "Upgrade {} {} {} {}".format(
            self.computer_cn, self.date, self.pkg, self.version
        )
