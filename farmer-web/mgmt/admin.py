from django.contrib import admin

from . import models

admin.site.register(models.ComputerAddress)
admin.site.register(models.ComputerAssignment)
admin.site.register(models.ErrorLog)
admin.site.register(models.LdapComputer)
admin.site.register(models.LdapUser)
admin.site.register(models.Role)
admin.site.register(models.RoleAssignment)
admin.site.register(models.Statistic)
admin.site.register(models.StorageServer)
admin.site.register(models.StorageServerAssignment)
