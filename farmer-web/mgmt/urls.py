from django.urls import path

from . import views


app_name = 'mgmt'

urlpatterns = [
    # HTML / redirects
    path('', views.home, name="home"),
    path(
        'adm/edit_computer/<computer_cn>', views.edit_computer,
        name='edit_computer'
    ),

    path('adm/clear_errors', views.clear_errors, name='clear_errors'),
    path('adm/create_computer', views.create_computer, name='create_computer'),
    path('adm/create_server', views.create_server, name='create_server'),
    path('adm/create_user', views.create_user, name='create_user'),
    path('adm/create_variable', views.create_variable, name='create_variable'),
    path(
        'adm/create_variable/<computer_cn>', views.create_variable,
        name='create_variable'
    ),
    path(
        'adm/edit_computer/<computer_cn>', views.edit_computer,
        name='edit_computer'
    ),
    path('adm/edit_server/<server_id>', views.edit_server, name='edit_server'),
    path('adm/edit_user/<user_cn>', views.edit_user, name='edit_user'),
    path(
        'adm/edit_variable/<variable_id>', views.edit_variable,
        name='edit_variable'
    ),
    path(
        'adm/delete_variable/<variable_id>',
        views.delete_variable, name='delete_variable'
    ),
    path('adm/list_computers', views.list_computers, name='list_computers'),
    path('adm/list_errors', views.list_errors, name='list_errors'),
    path('adm/list_roles', views.list_roles, name='list_roles'),
    path('adm/list_servers', views.list_servers, name='list_servers'),
    path('adm/list_users', views.list_users, name='list_users'),
    path('adm/list_variables', views.list_variables, name='list_variables'),
    path('adm/reload_roles', views.reload_roles, name='reload_roles'),
    path(
        'adm/show_computer_dmesgs/<computer_cn>', views.show_computer_dmesgs,
        name='show_computer_dmesgs'
    ),
    path(
        'adm/show_computer_errors/<computer_cn>', views.show_computer_errors,
        name='show_computer_errors'
    ),
    path(
        'adm/show_computer_upgrades/<computer_cn>',
        views.show_computer_upgrades,
        name='show_computer_upgrades'
    ),
    path(
        'adm/show_stat/<computer_cn>/<stat_id>',
        views.show_stat, name='show_stat'
    ),
    path('adm/show_dmesg/<dmesg_id>', views.show_dmesg, name='show_dmesg'),
    path('adm/show_error/<error_id>', views.show_error, name='show_error'),
    path('adm/show_stats/<computer_cn>', views.show_stats, name='show_stats'),

    # HTML / redirects | allowed to simple users
    path('adm/show_user', views.show_user, name='show_user'),

    # JSON | allowed to computers
    path('get_computer_vars/<computer_cn>', views.get_computer_vars),
    path('get_playbook_inventory/<computer_cn>', views.get_playbook_inventory),
    path('post_address/<computer_cn>', views.post_address),
    path('post_stats/<computer_cn>', views.post_stats),
    path('post_error/<computer_cn>', views.post_error),
    path('post_dmesg/<computer_cn>', views.post_dmesg),
    path('post_upgrade/<computer_cn>', views.post_upgrade),
]
