#!/bin/bash

if [ -f /etc/openvpn/dh2048.pem ] ; then
  exit 0
fi

export EASYRSA_BATCH=1

rm -rf /etc/openvpn/easy-rsa
cp -Ra /usr/share/easy-rsa /etc/openvpn
cp -f /etc/openvpn/vars /etc/openvpn/easy-rsa/vars

cd /etc/openvpn/easy-rsa

./easyrsa init-pki
./easyrsa build-ca nopass
./easyrsa gen-dh
./easyrsa gen-req openvpn-server nopass
./easyrsa sign-req server openvpn-server

cp -f pki/ca.crt /etc/openvpn
cp -f pki/private/ca.key /etc/openvpn
cp -f pki/issued/openvpn-server.crt /etc/openvpn/server.crt
cp -f pki/private/openvpn-server.key /etc/openvpn/server.key
cp -f pki/dh.pem /etc/openvpn/dh2048.pem
