#!/usr/bin/env python3

import sys
import uuid

import gi
gi.require_version('NM', '1.0')
from gi.repository import GLib
from gi.repository import NM


main_loop = None


def added_cb(client, result, data):
    try:
        client.add_connection_finish(result)
        print("The connection profile has been successfully added to NetworkManager.")
    except Exception as e:
        sys.stderr.write("Error: %s\n" % e)
    main_loop.quit()


def main():
    global main_loop

    target_ip = sys.argv[1]
    login = sys.argv[2]
    passwd = sys.argv[3]

    vpn = NM.SimpleConnection.new()

    s_conn = NM.SettingConnection.new()
    s_conn.set_property(NM.SETTING_CONNECTION_ID, "VPN POUET")
    s_conn.set_property(NM.SETTING_CONNECTION_UUID, str(uuid.uuid4()))
    s_conn.set_property(NM.SETTING_CONNECTION_TYPE, "vpn")
    s_conn.set_property(NM.SETTING_CONNECTION_PERMISSIONS, [
        "user:{}:".format(login)
    ])
    s_conn.set_property(NM.SETTING_CONNECTION_TIMESTAMP, 0)

    s_vpn = NM.SettingVpn.new()
    s_vpn.set_property(NM.SETTING_VPN_SECRETS, {
        "password": passwd,
    })
    s_vpn.set_property(
        NM.SETTING_VPN_SERVICE_TYPE,
        "org.freedesktop.NetworkManager.openvn"
    )
    s_vpn.set_property(NM.SETTING_VPN_DATA, {
        "ca": "/etc/pytux/vpn_ca.crt",
        "connection-type": "password",
        "password-flags": '1',
        'remote': target_ip,
        "username": login,
    })
    s_vpn.set_property(NM.SETTING_VPN_USER_NAME, login)

    s_ip4 = NM.SettingIP4Config.new()
    s_ip4.set_property(NM.SETTING_IP_CONFIG_METHOD, "auto")

    s_ip6 = NM.SettingIP6Config.new()
    s_ip6.set_property(NM.SETTING_IP_CONFIG_METHOD, "auto")

    vpn.add_setting(s_conn)
    vpn.add_setting(s_ip4)
    vpn.add_setting(s_ip6)
    vpn.add_setting(s_vpn)

    main_loop = GLib.MainLoop()

    client = NM.Client.new(None)

    # send the connection to NM
    client.add_and_activate_connection_async(vpn, None, added_cb, None)


if __name__ == "__main__":
    main()
