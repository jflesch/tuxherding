#!/usr/bin/env python3

import pydbus

DBUS_INTERFACE_NM = "org.freedesktop.NetworkManager"


def main():
    bus = pydbus.SystemBus()
    nm = bus.get(DBUS_INTERFACE_NM, "/org/freedesktop/NetworkManager/Settings")
    for conn in nm.ListConnections():
        print()
        print("======= {} =======".format(conn))
        conn = bus.get(DBUS_INTERFACE_NM, conn)

        try:
            print(conn.GetSettings()['vpn']['data']['remote'])
        except KeyError:
            pass

        print("==== Settings")
        for s in conn.GetSettings().items():
            print(s)
        try:
            print("==== Secrets")
            for s in conn.GetSecrets("vpn").items():
                print(s)
        except Exception as exc:
            print("Secrets: Exception: {}".format(exc))


if __name__ == "__main__":
    main()
