#!/usr/bin/env python3

import os
import sys

import pydbus


def main(login, passwd):
    bus = pydbus.SystemBus()
    tux = bus.get("computer.flesch.Pytux")
    r = tux.CheckPassword(login, passwd)
    print(r)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
