import datetime
import json
import logging
import re
import subprocess


LOGGER = logging.getLogger(__name__)

URL_POST_DMESG = '/mgmt/post_dmesg/{}'
DMESG_LINE_REGEX = re.compile("^\[\s*(?P<time>\d+\.\d+)\](?P<line>.*)$")


def exec_process(cmdline, input=None, **kwargs):
    try:
        sub = subprocess.Popen(
            cmdline, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT, **kwargs
        )
        (stdout, stderr) = sub.communicate(input=input)
        returncode = sub.returncode
    except OSError as e:
        raise
    if returncode != 0:
        raise RuntimeError(
            'Got return value %d while executing "%s", stderr output was:\n%s'
            % (returncode, " ".join(cmdline), stderr.rstrip("\n"))
        )
    return stdout


def load_dmesg():
    now = datetime.datetime.now()
    uptime_diff = None

    with open('/proc/uptime') as fd:
        uptime_diff = fd.read().strip().split()[0]
    uptime = now - datetime.timedelta(
        seconds=int(uptime_diff.split('.')[0]),
        microseconds=int(uptime_diff.split('.')[1])
    )

    dmesg_data = exec_process(['dmesg'])
    dmesg_data = dmesg_data.decode('utf-8')
    for line in dmesg_data.split('\n'):
        if not line:
            continue
        match = DMESG_LINE_REGEX.match(line)
        if match:
            try:
                seconds = int(match.groupdict().get('time', '').split('.')[0])
                nanoseconds = int(
                    match.groupdict().get('time', '').split('.')[1]
                )
                microseconds = int(round(nanoseconds * 0.001))
                line = match.groupdict().get('line', '')
                t = uptime + datetime.timedelta(
                    seconds=seconds, microseconds=microseconds
                )
                yield (t.isoformat(), line)
            except IndexError:
                pass


class Dmesg(object):
    def __init__(self, config, security_manager):
        self.config = config
        self.security_manager = security_manager

        self._load_and_send_dmesg()

    def _send_dmesg(self, dmesg):
        self.security_manager.post(
            URL_POST_DMESG.format(self.config['remote']['login']),
            {'log': json.dumps(dmesg)},
        )

    def _load_and_send_dmesg(self):
        LOGGER.info("[dmesg] Load dmesg ...")
        dmesg = list(load_dmesg())
        LOGGER.info("[dmesg] Post dmesg ...")
        self._send_dmesg(dmesg)
        LOGGER.info("[dmesg] All done")
