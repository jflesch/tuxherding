import logging
import os
import json
import re
import subprocess

import pySMART
import psutil


LOGGER = logging.getLogger(__name__)
# wait at least 15 min after boot before collecting stats
URL_POST_STATS = "/mgmt/post_stats/{}"


class Statistics(object):
    def __init__(self, config, security_manager, log):
        self.config = config
        self.security_manager = security_manager
        self.log = log

        self._post_stats()

    def _post_stats(self):
        stats = self._collect_stats()
        self._publish_stats(stats)

    @staticmethod
    def get_uptime():
        with open('/proc/uptime', 'r') as f:
            return int(float(f.readline().split()[0]))

    def _get_sensor_stats(self):
        if not hasattr(psutil, 'sensors_temperatures'):
            return {}

        stats = {}
        idx = 0

        temps = psutil.sensors_temperatures()
        for (chip, sensors) in temps.items():
            for sensor in sensors:
                name = "/sensor/temp/" + str(chip)
                if sensor.label != "":
                    name += "/" + sensor.label
                else:
                    name += "/" + str(idx)
                if sensor.current is not None:
                    stats[name + "/current"] = sensor.current
                if sensor.critical is not None:
                    stats[name + "/critical"] = sensor.critical
                if sensor.high is not None:
                    stats[name + "/high"] = sensor.high
                idx += 1

        fans = psutil.sensors_fans()
        for (chip, sensors) in fans.items():
            for sensor in sensors:
                name = "/sensor/fan/" + str(chip)
                if sensor.label != "":
                    name += "/" + sensor.label
                else:
                    name += "/" + str(idx)
                stats[name + "/current"] = sensor.current
                idx += 1

        return stats

    def _get_smart_stats(self):
        stats = {}
        for dev in pySMART.DeviceList().devices:
            v = 1 if dev.assessment == "PASS" else 0
            stats["/disk/" + dev.name + "/assessment"] = v

            for attr in dev.attributes:
                if attr is None or "unknown_attr" in attr.name.lower():
                    continue
                stats[
                        "/disk/" + dev.name + "/attr/" + attr.name.lower()
                        + "/value"
                ] = attr.value
                stats[
                        "/disk/" + dev.name + "/attr/" + attr.name.lower()
                        + "/raw"
                ] = attr.raw
                stats[
                        "/disk/" + dev.name + "/attr/" + attr.name.lower()
                        + "/worst"
                ] = attr.worst
        return stats

    def _get_mem_stats(self):
        if not hasattr(psutil, 'virtual_memory'):
            return {}
        if not hasattr(psutil, 'swap_memory'):
            return {}

        vm = psutil.virtual_memory()
        swap = psutil.swap_memory()
        return {
            "/mem/ram/total": vm.total / 1024 / 1024,
            "/mem/ram/used": vm.used / 1024 / 1024,
            "/mem/ram/free": vm.available / 1024 / 1024,
            "/mem/swap/total": swap.total / 1024 / 1024,
            "/mem/swap/used": swap.used / 1024 / 1024,
            "/mem/swap/free": swap.free / 1024 / 1024,
        }

    def _get_partition_stats(self):
        if not hasattr(psutil, 'disk_partitions'):
            return {}

        stats = {}
        parts = psutil.disk_partitions()
        for part in parts:
            name = part.device.replace("/dev/", "")
            if name[:2] != "sd":
                continue
            mnt = part.mountpoint
            if mnt is None:
                continue
            part = psutil.disk_usage(mnt)
            # crappy way to return the mount point ...
            stats["/partition/" + name + "/mountpoint" + mnt] = 0
            stats["/partition/" + name + "/total"] = part.total / 1024 / 1024
            stats["/partition/" + name + "/free"] = part.free / 1024 / 1024
            stats["/partition/" + name + "/used"] = part.used / 1024 / 1024
        return stats

    def _get_battery(self):
        stats = {}
        for batnum in range(2):
            os.environ['LANG'] = 'C'
            r = subprocess.run(
                [
                    'upower',
                    '-i',
                    '/org/freedesktop/UPower/devices/battery_BAT{}'.format(
                        batnum
                    )
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT
            )
            if r.returncode != 0:
                return stats
            stdout = r.stdout.decode('utf-8')
            lines = stdout.split('\n')
            stdout = {}
            for line in lines:
                line = line.split(":", 1)
                if len(line) < 2:
                    continue
                line = [l.strip() for l in line]
                line[0] = line[0].replace(" ", "_")
                if line[1] == "yes":
                    line[1] = 1
                elif line[1] == "no":
                    line[1] = 0
                else:
                    line[1] = re.sub("[^0-9.]", "", line[1])
                    try:
                        line[1] = int(float(line[1]))
                    except ValueError:
                        continue
                stats['/battery/{}/{}'.format(batnum, line[0])] = line[1]

        if hasattr(psutil, 'sensors_battery'):
            battery = psutil.sensors_battery()
            if battery is not None:
                stats.update({
                    '/battery/0/remaining/percent': int(battery.percent),
                    '/battery/0/remaining/mins':
                        int(battery.secsleft / 60)
                        if battery.secsleft > 0
                        else battery.secsleft,
                    '/battery/0/power_plugged':
                        1 if battery.power_plugged else 0,
                })
        return stats

    def _get_stats(self, name, callback):
        try:
            return callback()
        except Exception as exc:
            LOGGER.error(
                "[stats] Failed to get stats '%s'",
                name, exc_info=exc
            )
            self.log.post_error()
        return {}

    def _collect_stats(self):
        stats = {
            "/boot/uptime": self.get_uptime(),
        }
        stats.update(self._get_stats("sensors", self._get_sensor_stats))
        stats.update(self._get_stats("SMART", self._get_smart_stats))
        stats.update(self._get_stats("memory", self._get_mem_stats))
        stats.update(self._get_stats("partitions", self._get_partition_stats))
        stats.update(self._get_stats("battery", self._get_battery))

        for (k, v) in stats.items():
            LOGGER.info("[stats] Stat: {} = {}".format(k, v))
        return stats

    def _publish_stats(self, stats):
        stats = [{"name": s[0], "value": int(s[1])} for s in stats.items()]
        args = {'stats': json.dumps(stats)}
        addr = URL_POST_STATS.format(self.config['remote']['login'])
        self.security_manager.post(addr, args)
