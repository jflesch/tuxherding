import http.client as http
import json
import logging
import random
import string
import urllib.parse


LOGGER = logging.getLogger(__name__)
URL_GET_USER_INFO = "/mgmt/get_user_info"


class SecurityManager(object):
    def __init__(self, config):
        self.tokens = {}
        self.config = config

    @staticmethod
    def generate_token(length=16):
        characters = string.ascii_letters + string.digits
        return "".join(random.choice(characters) for _ in range(length))

    def login(self, username, passwd):
        # TODO(Jflesch): check password

        for (token, user) in self.tokens.items():
            if user['login'] == username:
                return token

        token = self.generate_token(length=64)
        self.tokens[token] = {
            "login": username,
            "passwd": passwd,
        }
        return token

    def get_user_info(self, security_token):
        return self.tokens[security_token]

    def get_post_base_args(self):
        return {
            'computer_login': self.config['remote']['login'],
            'computer_passwd': self.config['remote']['passwd'],
        }

    def get_server_connection(self):
        kls = http.HTTPSConnection
        if self.config['remote']['protocol'] == "http":
            kls = http.HTTPConnection
        return kls(
            self.config['remote']['server'],
            port=int(self.config['remote']['port'])
        )

    def post(self, url, url_args):
        args = self.get_post_base_args()
        args.update(url_args)
        args = urllib.parse.urlencode(args)
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "text/plain",
        }
        conn = self.get_server_connection()
        LOGGER.info("[security] POST to {}".format(url))
        conn.request("POST", url, args, headers)
        response = conn.getresponse()
        r = response.read()
        LOGGER.info("[security] Response: {}".format(response.status))
        if response.status != 200:
            raise Exception("Failed to post to {}: {}".format(
                url, response.status
            ))
        return r.decode('utf-8')

    def get_sync_info(self, security_token):
        user = self.tokens[security_token]
        args = {
            'user_login': user['login'],
            'user_passwd': user['passwd'],
        }
        r = self.post(URL_GET_USER_INFO, args)
        out = json.loads(r)
        return {
            'user': user,
            'remotes': out,
        }
        return out
