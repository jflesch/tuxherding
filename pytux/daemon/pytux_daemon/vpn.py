import logging
import os
import stat
import socket
import subprocess
import threading
import time

from . import config


LOGGER = logging.getLogger(__name__)
DBUS_INTERFACE_NM = "org.freedesktop.NetworkManager"

CONFIG_FILE = "/etc/pytux/openvpn.conf"
AUTH_FILE = "/etc/pytux/openvpn_auth.txt"

SERVER_VPN_IP = '172.29.0.1'

URL_POST_ADDRESS = "/mgmt/post_address/{}"


class Vpn(object):
    def __init__(self, bus, config, security_manager):
        self.bus = bus
        self.config = config
        self.security_manager = security_manager
        self.openvpn = None
        self.status_callback = None

    def enable(self):
        self.disable()
        LOGGER.info("[vpn] Enabling VPN")

        with open(AUTH_FILE, "w") as fd:
            os.fchmod(fd.fileno(), stat.S_IRWXU)
            fd.write("{}\n".format(self.config['remote']['login']))
            fd.write("{}\n".format(self.config['remote']['passwd']))

        with open(CONFIG_FILE, "w") as fd:
            os.fchmod(fd.fileno(), stat.S_IRWXU)
            fd.write("""
client
dev tun
proto udp

remote {remote} 1194

resolv-retry infinite

nobind

persist-tun

ca {ca_file}

auth-user-pass {auth_file}
            """.format(
                remote=self.config['remote']['vpn'], ca_file=config.CA_CRT,
                auth_file=AUTH_FILE
            ))

        self.openvpn = subprocess.Popen(
            ['/usr/sbin/openvpn', '--config', CONFIG_FILE]
        )
        threading.Thread(target=self._run_openvpn).start()
        threading.Thread(target=self._publish_ip).start()

    def _run_openvpn(self):
        if self.status_callback is not None:
            self.status_callback(True)
        self.openvpn.communicate()
        if self.status_callback is not None:
            self.status_callback(False)
        self.openvpn = None

    def _publish_ip(self):
        for _ in range(0, 10):
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect((SERVER_VPN_IP, 80))
            our_ip = s.getsockname()[0]
            s.close()

            if our_ip.startswith("172."):
                break

            time.sleep(1)

        args = {'address': our_ip}
        addr = URL_POST_ADDRESS.format(self.config['remote']['login'])
        self.security_manager.post(addr, args)

    def disable(self):
        openvpn = self.openvpn
        if openvpn is not None:
            openvpn.kill()
            openvpn.wait()
            self.openvpn = None
