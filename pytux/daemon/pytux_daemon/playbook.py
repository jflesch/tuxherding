import hashlib
import logging
import os
import json
import stat
import shutil
import subprocess

import dulwich.porcelain


LOGGER = logging.getLogger(__name__)

FILE_LAST_SUCCESSFUL = "/var/pytux/last_successful"
FILE_INVENTORY = "/var/pytux/ansible_inventory"
LOCAL_REPOSITORY = "/var/pytux/ansible"
FILE_OUTPUT = "/tmp/ansible.out"
FILE_PLAYBOOK = "main.yaml"
URL_GET_INVENTORY = "/mgmt/get_playbook_inventory/{}"


class PlaybookManager(object):
    def __init__(self, config, security_manager):
        self.config = config
        self.security_manager = security_manager

        if ('services' in self.config and
                'playbook' in self.config['services'] and
                not bool(int(self.config['services']['playbook']))):
            LOGGER.info(
                "Playbook is disabled. Won't be updated and applied."
            )
        else:
            self._load_and_run_playbooks()

    def mkdir_p(self, path):
        try:
            os.mkdir(path)
        except FileExistsError:
            pass
        os.chmod(path, stat.S_IRWXU)

    def _load_inventory(self):
        addr = URL_GET_INVENTORY.format(self.config['remote']['login'])
        r = self.security_manager.post(addr, {})
        cvars = json.loads(r)

        self.mkdir_p(os.path.dirname(FILE_INVENTORY))

        with open(FILE_INVENTORY, "w") as fd:
            os.fchmod(fd.fileno(), stat.S_IRUSR | stat.S_IWUSR)
            fd.write("[all:vars]\n")
            for (k, v) in cvars.items():
                LOGGER.info("[playbook] {} = {}".format(k, str(v)))
                fd.write("{}={}\n".format(k, str(v)))

        return cvars

    def _get_repository(self, repository):
        self.mkdir_p(os.path.dirname(LOCAL_REPOSITORY))

        if os.path.exists(os.path.join(LOCAL_REPOSITORY, ".git")):
            try:
                repo = dulwich.porcelain.Repo(LOCAL_REPOSITORY)
                dulwich.porcelain.pull(repo, repository)
                return repo
            except Exception as exc:
                LOGGER.warning(
                    "Failed to pull from repository. Will clone",
                    exc_info=exc
                )
                shutil.rmtree(LOCAL_REPOSITORY)
        repo = dulwich.porcelain.clone(repository, LOCAL_REPOSITORY)
        return repo

    def _get_latest_successful(self):
        if not os.path.exists(FILE_LAST_SUCCESSFUL):
            return {'commit': '', 'inventory': ''}
        with open(FILE_LAST_SUCCESSFUL, 'r') as fd:
            lines = [l.strip() for l in fd.readlines()]
            if len(lines) <= 1:
                return {'commit': lines[0], 'inventory': ''}
            else:
                return {'commit': lines[0], 'inventory': lines[1]}

    def _write_latest_successful(self, commit, inventory):
        self.mkdir_p(os.path.dirname(FILE_LAST_SUCCESSFUL))
        with open(FILE_LAST_SUCCESSFUL, 'w') as fd:
            fd.write(commit + "\n")
            fd.write(inventory + "\n")

    def _write_main(self, inventory):
        self.mkdir_p(os.path.dirname(LOCAL_REPOSITORY))

        with open(os.path.join(LOCAL_REPOSITORY, FILE_PLAYBOOK), "w") as fd:
            os.fchmod(fd.fileno(), stat.S_IRUSR | stat.S_IWUSR)
            fd.write("""---
- hosts: localhost
  connection: local
  roles:
""")
            for role in inventory['roles']:
                fd.write("  - {}\n".format(role))

    def _run_playbook(self, path):
        with open(FILE_OUTPUT, 'w') as fd:
            r = subprocess.run(
                [
                    '/usr/bin/ansible-playbook',
                    '-i', FILE_INVENTORY,
                    path
                ],
                stdout=fd,
                stderr=subprocess.STDOUT
            )
        with open(FILE_OUTPUT, 'r') as fd:
            content = fd.readlines()
            for l in content:
                LOGGER.info("[eplaybook] %s", l.strip())
        if r.returncode != 0:
            raise Exception("Ansible failed: {}".format(r.returncode))

    def _load_and_run_playbooks(self):
        last_success = self._get_latest_successful()
        LOGGER.info(
            "[playbook] Lastest successful commit: %s",
            str(last_success)
        )
        LOGGER.info("[playbook] Loading inventory ...")
        inventory = self._load_inventory()
        inventory_hash = hashlib.sha256(
            json.dumps(inventory, sort_keys=True).encode('utf-8')
        ).hexdigest()
        LOGGER.info(
            "[playbook] Loading playbook from %s ...",
            inventory['repository']
        )
        repo = self._get_repository(inventory['repository'])
        latest_commit = repo.head().decode('utf-8')
        LOGGER.info(
            "Commit: %s ; Inventory: %s (%s)",
            latest_commit, str(inventory), inventory_hash
        )
        if (latest_commit == last_success['commit'] and
                inventory_hash == last_success['inventory']):
            LOGGER.info(
                "[playbook] Playbook hasn't changed. Nothing to update"
            )
            return
        LOGGER.info("[playbook] Writing %s", FILE_PLAYBOOK)
        self._write_main(inventory)
        LOGGER.info("[playbook] Running playbook")
        self._run_playbook(os.path.join(LOCAL_REPOSITORY, FILE_PLAYBOOK))
        self._write_latest_successful(latest_commit, inventory_hash)
        LOGGER.info("[playbook] All done")
