#!/usr/bin/env python3

import datetime
import logging
import os
import random
import sys
import threading
import time

from gi.repository import GLib

import pydbus

from . import config
from . import dmesg
from . import mail
from . import log
from . import playbook
from . import security
from . import stats
from . import upgrade
from . import vpn


LOGGER = logging.getLogger(__name__)

WAKEUP_INTERVAL = 60
MIN_UPTIME = 10 * 60
MAX_DELAY = 10 * 60


class Pytux(object):
    """
    <node>
        <interface name='computer.flesch.pytux.Pytux'>
            <method name='CheckPassword'>
                <arg type='s' name='login' direction='in'/>
                <arg type='s' name='password' direction='in'/>
                <arg type='b' name='valid' direction='out' />
                <arg type='s' name='securityToken' direction='out' />
            </method>

            <method name='StartVpn'>
            </method>
            <method name='GetVpnStatus'>
                <arg type='b' name='enabled' direction='out' />
            </method>
            <method name='StopVpn'>
            </method>
            <signal name="VpnStatus">
                <arg name='enabled' type='b'/>
            </signal>
        </interface>
    </node>
    """

    def __init__(self, security_manager, vpn):
        self.security_manager = security_manager
        self.vpn = vpn
        self.vpn_status = False

    def CheckPassword(self, login, password):
        LOGGER.info("[dbus] Login attempt: {}".format(login))
        token = self.security_manager.login(login, password)
        if token is None or token == "":
            LOGGER.error("[dbus] Login failed")
            return (False, "INVALID_LOGIN")
        LOGGER.info("[dbus] Login successful: {}".format(token))
        return (True, token)

    def StartVpn(self):
        LOGGER.info("[dbus] Start VPN")
        self.vpn.enable()

    def GetVpnStatus(self):
        return self.vpn_status

    def StopVpn(self):
        LOGGER.info("[dbus] Stop VPN")
        self.vpn.disable()

    VpnStatus = pydbus.generic.signal()

    def set_vpn_status(self, enabled):
        self.vpn_status = enabled
        self.VpnStatus(enabled)


class Core(object):
    def __init__(self, run_now=False):
        self.run_now = run_now
        self.cond = threading.Condition()
        self.running = True
        self.last_run_day = None

        self.loop = GLib.MainLoop()

        if os.getuid() == 0:
            self.bus = pydbus.SystemBus()
        else:
            self.bus = pydbus.SessionBus()

        self.config = config.read()
        self.security = security.SecurityManager(self.config)
        self.log = log.Log(self.config, self.security)
        self.vpn = vpn.Vpn(self.bus, self.config, self.security)
        self.dbus_obj = Pytux(self.security, self.vpn)

        self.vpn.status_callback = self.dbus_obj.set_vpn_status

        self.bus.publish("computer.flesch.Pytux", self.dbus_obj)

    def daily(self):
        try:
            dmesg.Dmesg(self.config, self.security)
        except Exception as exc:
            self.log.post_exception(exc)

        try:
            mail.MailManager(self.config, self.security)
        except Exception as exc:
            self.log.post_exception(exc)

        try:
            playbook.PlaybookManager(self.config, self.security)
        except Exception as exc:
            self.log.post_exception(exc)

        try:
            stats.Statistics(self.config, self.security, self.log)
        except Exception as exc:
            self.log.post_exception(exc)

        try:
            upgrade.UpgradeManager(self.config, self.security)
        except Exception as exc:
            self.log.post_exception(exc)

    def on_wakeup(self):
        # wait for the system to have been up and running for at least
        # MIN_UPTIME seconds
        if self.last_run_day is None and not self.run_now:
            uptime = stats.Statistics.get_uptime()
            if uptime < MIN_UPTIME:
                LOGGER.info(
                    "Uptime: %ds ; need at least %ds ; remaining: %ds",
                    uptime, MIN_UPTIME, MIN_UPTIME - uptime
                )
                return

        now = datetime.datetime.now()
        if now.day == self.last_run_day:
            # already run today
            return

        if self.last_run_day is not None and not self.run_now:
            delay = random.randint(0, MAX_DELAY)
            LOGGER.info("Will wait for %ds", delay)
            time.sleep(delay)

        self.last_run_day = now.day
        LOGGER.info("Running daily job")
        self.daily()

    def run(self):
        with self.cond:
            while self.running:
                self.on_wakeup()
                self.cond.wait(WAKEUP_INTERVAL)

    def start(self):
        threading.Thread(target=self.run).start()


def main():
    run_now = False
    if len(sys.argv) >= 2 and sys.argv[1] == "-n":
        run_now = True

    core = Core(run_now)
    core.start()
    LOGGER.info("Daemon ready")
    try:
        core.loop.run()
    finally:
        with core.cond:
            core.running = False
            core.cond.notify_all()
    LOGGER.info("Daemon stopped")


if __name__ == "__main__":
    main()
