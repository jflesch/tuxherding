#!/usr/bin/env python3


import configparser
import http.client as http
import logging
import os
import socket
import stat
import sys


LOGGER = logging.getLogger(__name__)
CONFIG_DIR = "/etc/pytux"
CONFIG_FILE = "/etc/pytux/pytux.conf"
CA_CRT = "/etc/pytux/vpn_ca.crt"

URL_VPN_CA_CRT = "/static/vpn_ca.crt"


def main():
    server = input("HTTP(S) Server ? ")
    if server.startswith("http://"):
        protocol = "http"
        server = server[len("http://"):]
    elif server.startswith("https://"):
        protocol = "https"
        server = server[len("https://"):]
    else:
        protocol = "https"
    if server[-1] == "/":
        server = server[:-1]
    if ":" in server:
        (server, port) = server.split(":")
        port = int(port)
    elif protocol == "http":
        port = 80
    else:
        port = 443

    port = int(port)

    login = input("Computer name ? [{}] ".format(socket.gethostname()))
    if login == "":
        login = socket.gethostname()

    passwd = input("Computer password ? ")
    vpn_server = input("VPN server ? ")

    if protocol == "http":
        conn = http.HTTPConnection(server, port=port)
    else:
        conn = http.HTTPSConnection(server, port=port)
    conn.request("GET", URL_VPN_CA_CRT)
    reply = conn.getresponse()
    if reply.status != 200:
        print("Failed to get {}://{}/{}: HTTP {}".format(
            protocol, server, URL_VPN_CA_CRT, reply.status
        ))
        sys.exit(1)
    ca_crt = reply.read()

    config = configparser.ConfigParser()
    config['remote'] = {
        'protocol': protocol,
        'server': server,
        'port': port,
        'login': login,
        'passwd': passwd,
        'vpn': vpn_server,
    }
    config['services'] = {
        'playbook': 1,
    }

    try:
        os.mkdir(CONFIG_DIR)
    except FileExistsError:
        pass
    os.chmod(
        CONFIG_DIR,
        (
            stat.S_IRWXU
            # | stat.S_IRGRP | stat.S_IXGRP
            # | stat.S_IROTH | stat.S_IXOTH
        )
    )

    with open(CONFIG_FILE, 'w') as fd:
        os.fchmod(fd.fileno(), stat.S_IRWXU)
        config.write(fd)
    with open(CA_CRT, 'wb') as fd:
        os.fchmod(
            fd.fileno(),
            stat.S_IRWXU
            # | stat.S_IRGRP | stat.S_IROTH
        )
        fd.write(ca_crt)


def read():
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    return config


if __name__ == "__main__":
    main()
