import datetime
import json
import logging
import os
import re
import stat


LOGGER = logging.getLogger(__name__)

URL_POST_UPGRADE = "/mgmt/post_upgrade/{}"

UPGRADE_HISTORY = "/var/log/apt/history.log"
UPGRADE_DATE_FORMAT = "%Y-%m-%d  %H:%M:%S"
UPGRADE_PKG_RE = re.compile("[\w.:-]+ \([\w.:, -]+\)")
UPGRADE_LAST_PKG = "/var/pytux/last_pkg"

DATE_ISO_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"


class UpgradeManager(object):
    def __init__(self, config, security_manager):
        self.config = config
        self.security_manager = security_manager

        if not os.path.exists(UPGRADE_HISTORY):
            LOGGER.info("[upgrades] no upgrade log")
            return

        self._load_and_send_upgrades()

    def _parse_packages(self, pkg_str, version_idx=0, forced_version=None):
        for pkg in UPGRADE_PKG_RE.findall(pkg_str):
            (pkg, version) = pkg.split(" ", 1)
            if forced_version is not None:
                version = forced_version
            else:
                version = version[1:-1]  # drop the '(' ')'
                if ", " in version:
                    # only the current version matters to us
                    version = version.split(", ")[version_idx]
            yield (pkg, version)

    def _load_upgrades(self):
        with open(UPGRADE_HISTORY, 'r') as fd:
            lines = [line.strip() for line in fd.readlines()]

        # group upgrades lines together
        upgrade = {}
        upgrades = []
        for line in lines:
            if line == "":
                if len(upgrade) > 0:
                    upgrades.append(upgrade)
                upgrade = {}
                continue
            (k, v) = line.split(": ", 1)
            upgrade[k] = v
        if len(upgrade) > 0:
            upgrades.append(upgrade)

        for upgrade in upgrades:
            if 'End-Date' not in upgrade:
                LOGGER.warning("Missing end-date: %s", str(upgrade))
                continue
            date = datetime.datetime.strptime(
                upgrade['End-Date'], UPGRADE_DATE_FORMAT
            )
            pkgs = []
            command_line = None
            if 'Commandline' in upgrade:
                command_line = upgrade['Commandline']
            if 'Upgrade' in upgrade:
                pkgs += list(
                    self._parse_packages(upgrade['Upgrade'], version_idx=1)
                )
            elif 'Install' in upgrade:
                pkgs += list(
                    self._parse_packages(upgrade['Install'], version_idx=0)
                )
            elif 'Purge' in upgrade:
                pkgs += list(
                    self._parse_packages(upgrade['Purge'], forced_version='')
                )
            elif 'Remove' in upgrade:
                pkgs += list(
                    self._parse_packages(upgrade['Remove'], forced_version='')
                )
            else:
                LOGGER.warning("Unknown action: %s", str(upgrade))
                continue
            for (pkg, version) in pkgs:
                yield (date, pkg, version, command_line)

    def mkdir_p(self, path):
        try:
            os.mkdir(path)
        except FileExistsError:
            pass
        os.chmod(path, stat.S_IRWXU)

    def _filter_upgrades(self, upgrades):
        if not os.path.exists(UPGRADE_LAST_PKG):
            return upgrades

        with open(UPGRADE_LAST_PKG, 'r') as fd:
            limit = datetime.datetime.strptime(fd.read(), DATE_ISO_FORMAT)

        if len(upgrades) <= 0 or upgrades[-1][0] == limit:
            return []

        for (idx, upgrade) in enumerate(upgrades):
            if upgrade[0] <= limit:
                continue
            return upgrades[idx:]
        return upgrades

    def _send_upgrade(self, date, pkgs, cmd):
        if len(pkgs) <= 0:
            return
        date = date.strftime(DATE_ISO_FORMAT)
        self.security_manager.post(
            URL_POST_UPGRADE.format(self.config['remote']['login']),
            {
                'date': date,
                'pkgs': json.dumps(pkgs),
                'command': cmd,
            }
        )

    def _send_upgrades(self, upgrades):
        # group back updates by date
        date = upgrades[0][0]
        cmd = upgrades[0][3]
        group = []
        for upgrade in upgrades:
            if upgrade[0] == date and upgrade[3] == cmd:
                group.append((upgrade[1], upgrade[2]))
            else:
                self._send_upgrade(date, group, cmd)
                cmd = upgrade[3]
                date = upgrade[0]
                group = [(upgrade[1], upgrade[2])]
        self._send_upgrade(date, group, cmd)

    def _mark_upgrades_sent(self, upgrades):
        date = upgrades[-1][0].strftime(DATE_ISO_FORMAT)

        self.mkdir_p(os.path.dirname(UPGRADE_LAST_PKG))
        with open(UPGRADE_LAST_PKG, 'w') as fd:
            fd.write(date)

    def _load_and_send_upgrades(self):
        LOGGER.info("[upgrades] Loading upgrades historic ...")
        upgrades = list(self._load_upgrades())
        upgrades.sort()
        LOGGER.info("[upgrades] Filtering already-submitted upgrades ...")
        upgrades = self._filter_upgrades(upgrades)
        LOGGER.info("[upgrades] %d new upgrades found", len(upgrades))
        if len(upgrades) > 0:
            LOGGER.info("[upgrades] Sending upgrades ...")
            self._send_upgrades(upgrades)
            self._mark_upgrades_sent(upgrades)
        LOGGER.info("[upgrades] All done")
