import logging
import sys
import threading


LOGGER = logging.getLogger(__name__)
URL_POST_ERROR = "/mgmt/post_error/{}"


def setup_thread_excepthook_workaround():
    """
    Workaround for `sys.excepthook` thread bug from:
    http://bugs.python.org/issue1230540

    Call once from the main thread before creating any threads.
    """
    init_original = threading.Thread.__init__

    def init(self, *args, **kwargs):
        init_original(self, *args, **kwargs)
        run_original = self.run

        def run_with_except_hook(*args2, **kwargs2):
            try:
                run_original(*args2, **kwargs2)
            except Exception:
                sys.excepthook(*sys.exc_info())

        self.run = run_with_except_hook

    threading.Thread.__init__ = init


class LogTracker(logging.Handler):
    # Assuming 1KB per line, it makes about 50MB of RAM
    # (+ memory allocator overhead)
    MAX_LINES = 50 * 1000
    LOG_LEVELS = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
    }

    def __init__(self):
        super().__init__()
        self._formatter = logging.Formatter(
            '%(levelname)-6s %(name)-30s %(message)s'
        )
        self.output = []
        self.config = None
        self.security_manager = None

    def emit(self, record):
        line = self._formatter.format(record)
        self.output.append(line)
        if len(self.output) > self.MAX_LINES:
            self.output.pop(0)

    def on_uncaught_exception_cb(self, exc_type, exc_value, exc_tb):
        self.post_exception((exc_type, exc_value, exc_tb))

    def post_exception(self, exc):
        LOGGER.error("=== UNCAUGHT EXCEPTION ===", exc_info=exc)
        LOGGER.error("===========================")
        self.post_error()

    def post_error(self):
        """
        Takes all the logs up to now and post them on the server
        """
        try:
            if self.config is None or self.security_manager is None:
                LOGGER.critical(
                    "Cannot post error: config or security_manager not set yet"
                )
                return
            self.security_manager.post(
                URL_POST_ERROR.format(self.config['remote']['login']),
                {
                    'log': "\n".join(self.output),
                }
            )
        except Exception as exc:
            LOGGER.error("Exception while posting error", exc_info=exc)

    @staticmethod
    def init(config, security_manager):
        g_log_tracker.config = config
        g_log_tracker.security_manager = security_manager

        logger = logging.getLogger()  # get main logger
        handler = logging.StreamHandler()
        handler.setFormatter(g_log_tracker._formatter)
        logger.addHandler(handler)
        logger.addHandler(g_log_tracker)
        logger.setLevel(logging.INFO)
        sys.excepthook = g_log_tracker.on_uncaught_exception_cb
        setup_thread_excepthook_workaround()


g_log_tracker = LogTracker()


class Log(object):
    def __init__(self, config, security_manager):
        g_log_tracker.init(config, security_manager)

    def post_error(self):
        g_log_tracker.post_error()

    def post_exception(self, exc_info):
        g_log_tracker.post_exception(exc_info)
