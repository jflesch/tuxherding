import json
import mailbox
import logging
import os


LOGGER = logging.getLogger(__name__)
MAILBOX = "/var/mail/root"
URL_POST_MAIL = "/mgmt/post_error/{}"


class MailManager(object):
    def __init__(self, config, security_manager):
        self.config = config
        self.security_manager = security_manager

        if not os.path.exists(MAILBOX):
            LOGGER.info("[mail] No mail for root")
            return

        self._load_and_send_mails()

    def _get_mails(self):
        LOGGER.info("[mail] Reading root mails")
        out = []
        box = mailbox.mbox(MAILBOX)
        for (mail_id, mail) in box.items():
            content = {k: v for (k, v) in mail.items()}
            content['BODY'] = mail.get_payload()
            out.append(content)
        LOGGER.info("[mail] {} mails found".format(len(out)))
        return out

    def _send_mail(self, mail):
        mail = json.dumps(mail)
        try:
            self.security_manager.post(
                URL_POST_MAIL.format(self.config['remote']['login']),
                {'log': mail}
            )
        except Exception as exc:
            LOGGER.error("[mail] Failed to post mail", exc_info=exc)
            return False
        return True

    def _load_and_send_mails(self):
        mails = self._get_mails()

        for mail in mails:
            if not self._send_mail(mail):
                LOGGER.error(
                    "[mail] Failed to submit one of the mails. Stopping"
                )
                return

        os.unlink(MAILBOX)
