#!/usr/bin/env python3

from setuptools import (
    find_packages,
    setup
)

setup(
    name="pytux-daemon",
    version="1.6.3",
    description="Pytux-daemon",
    long_description="Pytux daemon",
    classifiers=[
        "Intended Audience :: End Users/Desktop",
        ("License :: OSI Approved ::"
         " GNU General Public License v3 or later (GPLv3+)"),
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
    ],
    license="GPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@kwain.net",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'pytux-daemon = pytux_daemon.main:main',
            'pytux-config = pytux_daemon.config:main',
        ],
    },
    zip_safe=True,
    install_requires=[
        "dulwich",
        "pydbus",
        "psutil",
    ],
)
