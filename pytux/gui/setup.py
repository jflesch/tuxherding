#!/usr/bin/env python3

from setuptools import (
    find_packages,
    setup
)

setup(
    name="pytux-gui",
    version="1.3.0",
    description="Pytux-gui",
    long_description="Pytux GUI",
    classifiers=[
        "Intended Audience :: End Users/Desktop",
        ("License :: OSI Approved ::"
         " GNU General Public License v3 or later (GPLv3+)"),
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
    ],
    license="GPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@kwain.net",
    packages=find_packages(),
    package_dir={
        'pytux_gui': 'pytux_gui'
    },
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pytux-gui = pytux_gui.main:main',
        ],
    },
    zip_safe=True,
    install_requires=[
        "pydbus<0.7",
    ],
)
