#!/usr/bin/env python3

import os
import pkg_resources
import pydbus

from gi.repository import GLib
from gi.repository import Gtk

from . import vpn


def _get_resource_path(filename, pkg="pytux_gui"):
    path = pkg_resources.resource_filename(pkg, filename)
    if not os.access(path, os.R_OK):
        raise FileNotFoundError(
            "Can't find resource file '%s'. Aborting" % filename
        )
    return path


def load_uifile(filename):
    """
    Load a .glade file and return the corresponding widget tree

    Arguments:
        filename -- glade filename to load.

    Returns:
        GTK Widget tree

    Throws:
        Exception -- If the file cannot be found
    """
    widget_tree = Gtk.Builder()
    ui_file = _get_resource_path(filename)
    widget_tree.add_from_file(ui_file)
    return widget_tree


def main():
    main_loop = GLib.MainLoop()

    bus = pydbus.SystemBus()
    widget_tree = load_uifile("mainwin.xml")

    security_token = os.environ.get('SECURITY_TOKEN', "")
    if security_token == "":
        print("WARNING: Security token is not defined")

    vpn.Vpn(bus, widget_tree)

    window = widget_tree.get_object("appWindow")
    window.connect("destroy", lambda w: main_loop.quit())
    window.set_visible(True)

    main_loop.run()


if __name__ == "__main__":
    main()
