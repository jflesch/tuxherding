import base64
import platform
import subprocess

from gi.repository import GLib


DBUS_PYTUX = "computer.flesch.Pytux"

VNC_DCONF_SETTINGS = (
    ("/org/gnome/desktop/remote-access/authentication-methods", "['vnc']"),
    # let disable-background to false for Debian Stratch:
    # 'true' crashes on Debian Stretch
    (
        "/org/gnome/desktop/remote-access/disable-background",
        "true" if platform.dist() != ("debian", "9.9", "") else "false"
    ),
    ("/org/gnome/desktop/remote-access/network-interface", "''"),
    ("/org/gnome/desktop/remote-access/notify-on-connect", "true"),
    ("/org/gnome/desktop/remote-access/prompt-enabled", "false"),
    ("/org/gnome/desktop/remote-access/require-encryption", "false"),
    ("/org/gnome/desktop/remote-access/view-only", "false"),
    (
        "/org/gnome/desktop/remote-access/vnc-password",
        "'" + base64.encodebytes(b'support').decode('utf-8').strip() + "'",
    ),
)

VNC_COMMAND_START = ['systemctl', '--user', 'start', 'vino-server']
VNC_COMMAND_STOP = ['systemctl', '--user', 'stop', 'vino-server']


class Vpn(object):
    def __init__(self, bus, widget_tree):
        self.bus = bus
        self.widget_tree = widget_tree

        pytux = self.bus.get(DBUS_PYTUX)
        pytux.VpnStatus.connect(lambda state: self.update_vpn_state())

        self.vpn_state = False
        self.switch_signal = None

        self.update_vpn_state()

    def update_vpn_state(self):
        print("Checking VPN state ...")
        msg = "Désactivée"

        pytux = self.bus.get(DBUS_PYTUX)
        state = pytux.GetVpnStatus()

        if state:
            msg = "Activée"

        print("VPN state: {} ({})".format(state, msg))

        if self.switch_signal is not None:
            self.widget_tree.get_object("switchVpn").disconnect(
                self.switch_signal
            )
        self.widget_tree.get_object("switchVpn").set_active(state)
        self.switch_signal = self.widget_tree.get_object("switchVpn").connect(
            "state-set", self.check_vpn_switch
        )

        self.widget_tree.get_object("labelVpn").set_text(msg)
        self.vpn_state = state

    def enable_vnc(self):
        for (key, value) in VNC_DCONF_SETTINGS:
            cmd = ['dconf', 'write', key, value]
            print(subprocess.run(cmd))
        print(
            subprocess.run(
                VNC_COMMAND_START, stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT
            )
        )

    def disable_vnc(self):
        print(subprocess.run(VNC_COMMAND_STOP))

    def check_vpn_switch(self, switch, state):
        state = switch.get_active()
        if self.vpn_state == state:
            # we are (probably) notified of a change we did
            return

        print("Wanted VPN state: {} --> {}".format(self.vpn_state, state))

        try:
            pytux = self.bus.get(DBUS_PYTUX)
            if state:
                self.enable_vnc()
                pytux.StartVpn()
            else:
                self.disable_vnc()
                pytux.StopVpn()
            self.vpn_state = state
        except Exception:
            GLib.idle_add(
                self.widget_tree.get_object("switchVpn").set_active,
                not state
            )
            raise
