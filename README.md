# TuxHerding

Manage a herd of desktop/laptop Linux computers ("tux") from a server
("farmer").

- tuxes: computers
- farmer: server
- pytux: client agent


## farmer-web

Server-side

Management interface:
  - manage tuxes
    - admin interface
    - Get status of tux (temperature, battery, etc)
    - Provide information to tuxes (on which server backups must go,
      script to run, etc)
  - manage tux's users
    - admin interface


## pytux-daemon

- Send statistics (computer health, etc) to the server
- Load Ansible playbook from server and run them
-

## pytux-gui

GTK applications that allows user to:
- request that pytux-daemon enables the VPN for remote assistance

